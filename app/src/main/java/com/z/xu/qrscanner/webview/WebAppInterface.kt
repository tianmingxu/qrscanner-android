package com.z.xu.qrscanner.webview

import android.content.Context
import android.content.Intent
import android.webkit.JavascriptInterface
import com.z.xu.qrscanner.MainActivity

/**
 * Created by kyo on 2017/08/10.
 */
class WebAppInterface(context: Context) {

    val mContext: Context = context

    @JavascriptInterface
    fun startQrCodeReader() {
        Intent(mContext, MainActivity::class.java).also {
            mContext.startActivity(it)
        }
    }

}