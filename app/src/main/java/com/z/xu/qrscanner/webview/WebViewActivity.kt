package com.z.xu.qrscanner.webview

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.z.xu.qrscanner.R
import kotlinx.android.synthetic.main.activity_webview.*

/**
 * Created by kyo on 2017/08/10.
 */
class WebViewActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        webView.settings.javaScriptEnabled = true
        webView.addJavascriptInterface(WebAppInterface(this), "Android")
        val data = "<input type=\"button\" value=\"Start QR Scanner from this Web Page\" onClick=\"startQrCodeReader('Hello Android!')\" />\n" +
                "\n" +
                "<script type=\"text/javascript\">\n" +
                "    function startQrCodeReader() {\n" +
                "        Android.startQrCodeReader();\n" +
                "    }\n" +
                "</script>"
        webView.loadData(data, "text/html; charset=utf-8", "UTF-8")
    }
}